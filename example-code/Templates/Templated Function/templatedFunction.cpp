#include <iostream>
using namespace std;

template <typename T>
T Sum( T numA, T numB )
{
    return numA + numB;
}

int main()
{
    int intA = 4, intB = 6;
    float floatA = 3.9, floatB = 2.5;
    string strA = "alpha", strB = "bet";

    cout << intA << " + " << intB
        << " = " << Sum( intA, intB ) << endl;

    cout << floatA << " + " << floatB
        << " = " << Sum( floatA, floatB ) << endl;

    cout << strA << " + " << strB
        << " = " << Sum( strA, strB ) << endl;
}
