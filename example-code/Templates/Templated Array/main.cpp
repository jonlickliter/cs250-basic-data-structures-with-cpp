#include "TemplatedArray.hpp"

#include <iostream>
using namespace std;

int main()
{
    TemplatedArray<string> myList( 10 );

    try
    {
        myList.RemoveFromBack();
    }
    catch( runtime_error ex )
    {
        cout << "ERROR: " << ex.what() << endl;
    }

    myList.PushToBack( "cat" );
    myList.PushToBack( "rat" );
    myList.PushToBack( "bat" );

    myList.Display();

    myList.RemoveFromBack();
    myList.Display();

    return 0;
}
