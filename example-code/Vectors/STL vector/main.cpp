#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
    // Vector
    vector<string> nameList;

    // Add to vector
    nameList.push_back( "Bob1" );
    nameList.push_back( "Bob2" );
    nameList.push_back( "Bob3" );
    nameList.push_back( "Bob4" );

    // Access element
    cout << nameList[0] << endl;

    cout << nameList.size() << endl;

    return 0;
}

