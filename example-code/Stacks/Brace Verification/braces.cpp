#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stack>
using namespace std;

void ReadScript( vector<string>& linesOfCode, const string& filename )
{
    ifstream input( filename );

    string buffer;
    while ( getline( input, buffer ) )
    {
        linesOfCode.push_back( buffer );
    }

    input.close();
}

void ValidateSyntax( const vector<string>& linesOfCode )
{
    stack<char> braceMatcher;
    
    for ( unsigned int line = 0; line < linesOfCode.size(); line++ )
    {
        // Check each letter in the line
        for ( unsigned int letter = 0; letter < linesOfCode[ line ].size(); letter++ )
        {
            if ( linesOfCode[ line ][ letter ] == '{' )
            {
                braceMatcher.push( '{' );
            }
            else if ( linesOfCode[ line ][ letter ] == '}' )
            {
                braceMatcher.pop();
            }
        }
    }

    if ( braceMatcher.size() == 0 )
    {
        cout << "Braces are balanced" << endl;
    }
    else
    {
        cout << "Compile error: Uneven braces!" << endl;
    }
}

int main()
{
    vector<string> program;

    ReadScript( program, "program.script" );

    ValidateSyntax( program );
    
    return 0;
}
