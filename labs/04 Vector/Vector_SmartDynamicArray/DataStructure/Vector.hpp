#ifndef _VECTOR_HPP
#define _VECTOR_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

class Vector
{
    private:
    /* Member Variables */
    //! A string pointer used to allocate memory for a dynamic array.
    string* m_data;

    //! Stores how many items have been added to the Vector; less than or equal to the m_arraySize.
    int m_itemCount;

    //! Stores how large the array currently is; update after Resize() is called.
    int m_arraySize;

    public:
    /* Member Functions */
    // Constructor and Destructor
    Vector();
    ~Vector();

    // Deal with end of vector
    void Push( const string& newItem );

    // Deal with middle of vector
    string Get( int index ) const;
    void Remove( int index );

    // Helper functions
    int Size() const;
    bool IsFull() const;
    bool IsEmpty() const;

    private:
    // Internal helper functions
    bool IsInvalidIndex( int index ) const;
    bool IsElementEmpty( int index );

    // Memory mangement
    void AllocateMemory( int newSize = 10 );
    void DeallocateMemory();
    void Resize();

    // Special function, since we haven't covered exceptions yet
    void Panic( string message ) const;
    void NotImplemented() const;

    friend class Tester;
};

#endif
