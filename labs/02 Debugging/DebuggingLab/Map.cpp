#include "Map.hpp"

#include <iostream>
#include <vector>
using namespace std;

Map::Map()
    : MAP_WIDTH(20), MAP_HEIGHT(10), EMPTYROOM(' '), WALL('+'),
        MOVE_UP('w'), MOVE_DOWN('s'), MOVE_LEFT('a'), MOVE_RIGHT('d')
{
}

void Map::GenerateMap()
{
    // Fill in all the rooms
    for ( int y = 0; y < MAP_HEIGHT; y++ )
    {
        for ( int x = 0; x < MAP_WIDTH; x++ )
        {
            m_tiles[x][y] = WALL;
        }
    }

    vector<Coords> roomCoords;

    // Choose random rooms
    int roomCount = rand() % 5 + 5;
    for ( int i = 0; i < roomCount; i++ )
    {
        Coords c;
        c.x = rand() % 18 + 1;
        c.y = rand() % 8 + 1;
        m_tiles[c.x][c.y] = EMPTYROOM;
        roomCoords.push_back( c );
    }

    // Connect all the rooms
    for ( unsigned int i = 0; i < roomCoords.size() - 1; i++ )
    {
        int x = roomCoords[i].x;
        int y = roomCoords[i].y;
        int targetX = roomCoords[i+1].x;
        int targetY = roomCoords[i+1].y;

        while ( x != targetX )
        {
            m_tiles[x][y] = EMPTYROOM;

            if ( x < targetX ) { x++; }
            else { x--; }
        }

        while ( y != targetY )
        {
            m_tiles[x][y] = EMPTYROOM;

            if ( y < targetY ) { y++; }
            else { y--; }
        }
    }

    // Place the player and treasure
    int randX = rand() % MAP_WIDTH;
    int randY = rand() % MAP_HEIGHT;

    while ( m_tiles[randX][randY] != EMPTYROOM )
    {
        randX = rand() % MAP_WIDTH;
        randY = rand() % MAP_HEIGHT;
    }

    m_player.x = randX;
    m_player.y = randY;

    randX = rand() % MAP_WIDTH;
    randY = rand() % MAP_HEIGHT;
    while ( !( m_tiles[randX][randY] == EMPTYROOM
            && randX != m_treasure.x && randY != m_treasure.y ) )
    {
        randX = rand() % MAP_WIDTH;
        randY = rand() % MAP_HEIGHT;
    }

    m_treasure.x = randX;
    m_treasure.y = randY;

    randX = rand() % MAP_WIDTH;
    randY = rand() % MAP_HEIGHT;
    while ( !( m_tiles[randX][randY] == EMPTYROOM
                && randX != m_treasure.x && randY != m_treasure.y
                && randX != m_player.x && randY != m_player.y ) )
    {
        randX = rand() % MAP_WIDTH;
        randY = rand() % MAP_HEIGHT;
    }

    m_enemy.x = randX;
    m_enemy.y = randY;
}

void Map::Draw()
{
    cout << endl;
    for ( int y = 0; y < MAP_HEIGHT; y++ )
    {
        cout << " ";
        for ( int x = 0; x < MAP_WIDTH; x++ )
        {
            if ( x == m_player.x && y == m_player.y )
            {
                cout << "@";
            }
            else if ( x == m_treasure.x && y == m_treasure.y )
            {
                cout << "$";
            }
            else if ( x == m_enemy.x && y == m_enemy.y )
            {
                cout << "&";
            }
            else
            {
                cout << m_tiles[x][y];
            }
        }
        cout << endl;
    }
}

void Map::MovePlayer( string direction )
{
    char firstLetter = direction[0];
    firstLetter = tolower( firstLetter );

    if ( firstLetter == MOVE_UP )
    {
        if ( m_player.y - 1 >= 0 && m_tiles[m_player.x][m_player.y - 1] == EMPTYROOM )
        {
            m_player.y--;
        }
    }
    else if ( firstLetter == MOVE_DOWN )
    {
        if ( m_player.y + 1 < 10 && m_tiles[m_player.x][m_player.y + 1] == EMPTYROOM )
        {
            m_player.y++;
        }
    }
    else if ( firstLetter == MOVE_RIGHT )
    {
        if ( m_player.x + 1 < 20 && m_tiles[m_player.x + 1][m_player.y] == EMPTYROOM )
        {
            m_player.x++;
        }
    }
    else if ( firstLetter == MOVE_LEFT )
    {
        if ( m_player.x - 1 >= 0 && m_tiles[m_player.x - 1][m_player.y] == EMPTYROOM )
        {
            m_player.x--;
        }
    }

    MoveGoblin();
}

void Map::MoveGoblin()
{
    int x = m_enemy.x;
    int y = m_enemy.y;

    int direction = rand() % 4;

    if ( direction == 0 )
    {
        x += 1;
    }
    else if ( direction == 1 )
    {
        x -= 1;
    }
    else if ( direction == 2 )
    {
        y += 1;
    }
    else
    {
        y -= 1;
    }

    if ( m_tiles[x][y] == EMPTYROOM )
    {
        m_enemy.x = x;
        m_enemy.y = y;
    }
}

bool Map::PlayerCollectTreasure()
{
    return ( m_player.x == m_treasure.x && m_player.y == m_treasure.y );
}

bool Map::GoblinGetPlayer()
{
    return ( m_player.x == m_enemy.x && m_player.y == m_enemy.y );
}

char Map::MoveUpKey()
{
    return MOVE_UP;
}

char Map::MoveDownKey()
{
    return MOVE_DOWN;
}

char Map::MoveLeftKey()
{
    return MOVE_LEFT;
}

char Map::MoveRightKey()
{
    return MOVE_RIGHT;
}

